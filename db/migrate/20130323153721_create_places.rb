class CreatePlaces < ActiveRecord::Migration
  def change
    create_table :places do |t|
      t.string :name
      t.text :description
      t.integer :status
      t.references :location
      t.references :user
      t.timestamps
    end
    add_index :places, :location_id
    add_index :places, :user_id
  end
end
