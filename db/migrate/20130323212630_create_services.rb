class CreateServices < ActiveRecord::Migration
  def change
    create_table :services do |t|
      t.string :name
      t.text :description
      t.integer :status
      t.decimal :price, precision: 9, scale: 2
      t.string :image
      t.references :place

      t.timestamps
    end
    add_index :services, :place_id
  end
end
