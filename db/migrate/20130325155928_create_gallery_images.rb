class CreateGalleryImages < ActiveRecord::Migration
  def change
    create_table :gallery_images do |t|
      t.string :image
      t.references :place
      t.timestamps
    end
    add_index :gallery_images, :place_id
  end
end
