class CreatePlaceImages < ActiveRecord::Migration
  def change
    create_table :place_images do |t|
      t.string :image
      t.references :place

      t.timestamps
    end
    add_index :place_images, :place_id
  end
end
