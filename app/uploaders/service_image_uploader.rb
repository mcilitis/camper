# encoding: utf-8

class ServiceImageUploader < CarrierWave::Uploader::Base
  include CarrierWave::MiniMagick
  include CarrierWave::MimeTypes

  storage :file

  def store_dir
    "public/uploads/#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
  end

  version :mini_thumb do
    process resize_to_fit: [50, 50]
  end

  version :thumb do
    process resize_to_fit: [280, 180]
  end

  version :main do
    process resize_to_fit: [720, 480]
  end
end
