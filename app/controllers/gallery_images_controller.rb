class GalleryImagesController < ApplicationController
  respond_to :js

  def create
    @gallery_image = GalleryImage.new(params[:gallery_image])

    if @gallery_image.save
      respond_with @gallery_image
    else
      
    end
  end

  def destroy
    gallery_image = GalleryImage.find(params[:id])
    gallery_image.destroy
  end
end
