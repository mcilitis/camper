class PlacesController < ApplicationController

  def index
    @places = current_user.places
  end

  def show
    @place = Place.find(params[:id])
    @service = Service.new
  end

  def new
    @place = Place.new
  end

  def create
    @place = Place.new(params[:place])
    @place.user_id = current_user.id

    if @place.save
      respond_to do |format|
        format.html { redirect_to place_path(@place), notice: 'Place created successfully!'}
        format.js
      end
    else
      format.html { render :new}
    end
  end

  def edit
    @place = Place.find(params[:id])
  end

  def update
    @place = Place.find(params[:id])

    if @place.update_attributes(params[:place])
      respond_to do |format|
        format.html { redirect_to place_path(@place), notice: 'Place updated successfully!' }
        format.js
      end
    else
      respond_to do |format|
        format.html { render :edit }
        format.js
      end
    end
  end

  def destroy
    @place = Place.find(params[:id])

    if @place.destroy
      respond_to do |format|
        format.html { redirect_to places_path, notice: 'Place deleted successfully!'}
        format.js
      end
    else
      respond_to do |format|
        format.html { redirect_to places_path, error: 'Error occurred!!'}
        format.js
      end
    end
  end
end
