class ServicesController < ApplicationController

  respond_to :js
  def new
    @service = Service.new
  end

  def create
    @service = Service.new(params[:service])

    if @service.save
      respond_with @service
    else
      render 
    end
  end

  def destroy
    service = Service.find(params[:id])
    @place = service.place

    if service.destroy
      respond_with @place
    else
      
    end
  end

  def edit
    @service = Service.find(params[:id])
    respond_with @service
  end
end
