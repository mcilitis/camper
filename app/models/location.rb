class Location < ActiveRecord::Base
  attr_accessible :name
  has_many :places

  class << self
    def avaliable_locations_for_select
      all
    end
  end
end
