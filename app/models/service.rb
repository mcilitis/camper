class Service < ActiveRecord::Base
  attr_accessible :description, :image, :name, :price, :status, :place_id

  STATUSES = %w(Draft Published Unavailable)
  mount_uploader :image, ServiceImageUploader
  belongs_to :place

  def status_name
    STATUSES[self.status]
  end
end
