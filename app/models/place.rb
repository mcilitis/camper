class Place < ActiveRecord::Base
  STATUSES = %w(Draft Published)

  attr_accessible :description, :name, :status, :image, :location_id

  mount_uploader :image, PlaceImageUploader

  has_many :images, class_name: 'PlaceImage'
  has_many :services
  has_many :gallery_images, dependent: :destroy
  belongs_to :location


  def status_to_word
    STATUSES[self.status]
  end

  def services_count
    self.services.count
  end

  def location_name
    if self.location
      self.location.name
    else
      "Not specified"
    end
  end
end
