Camper::Application.routes.draw do
  resources :places
  resources :services
  resources :gallery_images
    
  devise_for :users

  root :to => 'home#index'
end
